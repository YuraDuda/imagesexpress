/**
 * Created by yura on 13.07.17.
 */

var express = require("express");
var imageRouter = require("./routes/imageRouter");
var mongoose = require('mongoose');
    mongoose.Promise = require('bluebird');
var bodyParser = require("body-parser");
var path = require('path');
var multer = require('multer');


mongoose.connect('mongodb://localhost/myapp',{
    useMongoClient: true
});

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('assets'));
app.use(multer({dest:__dirname+'/assets/images/smile_phot'}).any());
app.set('view engine', 'ejs');


app.listen(3000,function () {
    console.log('started');
});


app.use("/image",imageRouter);

