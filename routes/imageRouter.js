/**
 * Created by yura on 13.07.17.
 */

var express = require("express");
var imageController = require("./../controllers/imageController");

var imageRouter = express.Router();

imageRouter.route("/")
    .get(imageController.get)
    .post(imageController.upload);

imageRouter.route("/list")
    .get(imageController.list);



module.exports = imageRouter;