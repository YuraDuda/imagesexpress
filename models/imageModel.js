/**
 * Created by yura on 13.07.17.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var imageModel = new Schema({
    name : String,
    generatedImageName : String,
    cropImagesArray: Array
});


module.exports = mongoose.model("Image",imageModel);