/**
 * Created by yura on 13.07.17.
 */
var fs = require('fs');
var gm = require('gm');
var uuid = require("node-uuid");


var Crop = function () {

  this.crop = function(n,filePath,saveFilePath,fileName,next) {

      gm(filePath+fileName).size(function(err, value){

      var retreat = 20;

      var heightImage = value.height + (value.height/n)+40;

      var partHeight = value.height;
      var partWidth = value.width/n;

      var widthImage = partWidth * n + (n * retreat);

      var objResult = {};

      objResult.image = uuid.v4() + '.jpg';
      objResult.arrayCropImages = [];

      gm(widthImage, heightImage, "#ffffff").write(saveFilePath + objResult.image, function (err) {

         var  arrCropImages = [];
          for(var i = 0; i < n; ++i) {

              var x = i == 0 ? 0 : i * partWidth;
              console.log(x);

              getCutAllImages(fileName, filePath, partWidth, partHeight, x ,n, arrCropImages, function (cropsImages) {
                  objResult.arrayCropImages = cropsImages;

                  var files = objResult.arrayCropImages.slice();
                  files.unshift(objResult.image);

                  createImage(n,0, files,saveFilePath,partHeight,partWidth,gm()).write(saveFilePath + objResult.image, function (err) {

                      next(objResult);

                  });
              });

          }

      });

    });

  };

};

function createImage(n,i, files,saveFilePath,height,width,gm) {

    if(n == i-1){
        return  gm.mosaic();
    }

    i+=1;

    var x = i == 1 ? 0 : (i-2) * width +  20 * (i-1);
    var y = i == 1 ? 0 : height / (n*(i-1));
    var gms = gm.in('-page', '+'+ x +'+'+ y).in(saveFilePath + files[i-1]);


    return createImage(n,i,files,saveFilePath,height,width,gms);

}

function getCutAllImages(fileName, filePath, partWidth, partHeight, x ,n, arrCropImages,cb) {

    cutImage(fileName,filePath,partWidth,partHeight,x,function (newFileName) {

        arrCropImages.push(newFileName);

        if(n == arrCropImages.length){
            cb(arrCropImages);
        }
    });
}

function cutImage(fileName, filePath, partWidth, partHeight, x, cb) {

    var newFileName = uuid.v4()+'.jpg';

    gm(filePath+fileName).crop(partWidth, partHeight, x, 0).write(filePath+newFileName, function (err) {
        cb(newFileName);
    });

}

module.exports = function () {
    return new Crop();
};

