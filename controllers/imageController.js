/**
 * Created by yura on 13.07.17.
 */

var Image = require("./../models/imageModel");

var fs = require('fs');
var gm = require('gm');
var path = require('path');
var uuid = require("node-uuid");
var multer  = require('multer');
var cropModule = require("./../crop");
var crop = new cropModule();



module.exports = {

    list : function (req,res) {
        res.render("image/list");
    },

    get : function (req,res) {
        Image.find(function (err,images) {
            if(err){
                res.status(500);
                res.send("Internal server error");
            }else {
                res.json({images : images});
            }
        })
    },

    upload : function (req,res) {
        var imageType = req.body.imageType;
        console.log(req.body);
        console.log(req.files);

        var nameFile = uuid.v4() + '.jpg';

            fs.rename(req.files[0].path, 'assets/images/smile_phot/'+nameFile,function (err) {
                if(err) throw new Error();

                crop.crop(imageType,'./assets/images/smile_phot/','./assets/images/smile_phot/',nameFile,function (objResult){

                    var image = new Image({
                        name : nameFile,
                        generatedImageName : objResult.image,
                        cropImagesArray : objResult.arrayCropImages
                    });
                    
                    image.save(function (req,saved) {
                        if(err){
                            res.status(500);
                            res.send("Internal server error");
                        }else{
                            console.log(saved);
                            res.json("success");
                        }
                    });
                });

            });
    }


};

